%% Ex 1
vecto = 31:75
%% Ex 2
vecto = randi([0,100], 1, 10)
%% Ex 3
vecto = randi([-20,10], 1, 10)
%% Ex 4
x = [3 1 5 7 9 2 6]
% a. Du doan ket qua = 5. Vi x(3) lay phan tu thu 3 cua vecto
x(3)
% b. Du doan ket qua la [3 1 5 7 9 2 6]. Vi x(1:7) lay phan tu thu 1 den 7
% cua vecto
x(1:7)
% c. Du doan ket qua la [3 1 5 7 9 2 6]. Vi x(1:end) lay phan tu thu 1 den
% ket thuc cua vecto
x(1:end)
% d. Du doan ket qua la loi(error) vi thua dau ")" o cuoi cau lenh.
%x(1:end-1) )
% e. Du doan ket qua la [2 7 1]. Vi lay phan tu thu 6 den phan tu thu nhat,
% buoc nhay la -1
x(6:-2:1)
% f. Du doan ket qua la [3 2 1 3 3]. Vi lay cac phan tu theo vi tri [1 6 2
% 1 1]
x([1 6 2 1 1])
% g. Du doan ket qua la 33. Vi ham sum(x) tinh tong cac phan tu cua x
sum(x)
%% Ex 5
x = [3 1 5 7 9 2 6]
% x(3) = 5
% x(1:7) = [3 1 5 7 9 2 6]
% x(1:end) = [3 1 5 7 9 2 6]
% x(1:end-1) = [3 1 5 7 9 2]
% x(6:-1:1) = [9 7 5 1 3]
% x([1 6 2 1 1]) = [3 2 1 3 3]
%% Ex 6
A = [ 2 4 1 ; 6 7 2 ; 3 5 9]
x1 = A(1, :)
y = A([end-1,end],:)
y = A([2 3],:)
y = A([end-1 end],:)
sum(A)
sum(A,2)
%% Ex 7
% a
